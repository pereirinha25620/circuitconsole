package view;

import isel.leic.pg.Console;
import isel.leic.pg.MouseEvent;
import model.Circuit;
import model.Position;
import view.cell.*;

import java.awt.event.KeyEvent;
import java.util.Collections;

public class Panel {

    CellView[][] cellViews;

    private long startTime = System.nanoTime();
    private long totalTime = 0, seconds = 0, minutes = 0;


    // Opens a new console window according to model's game grid
    public void open(Circuit model) {
        cellViews = new CellView[model.getLines()][model.getColumns()];

        Console.open("Circuit",model.getLines() * CellView.CELL_SIDE + 1, model.getColumns() * CellView.CELL_SIDE);
        Console.enableMouseEvents(true);

        generateCellViews(model);
        initTimer();
    }

    // Generates CellViews according to model's grid
    private void generateCellViews(Circuit model) {
        for (int l = 0; l < model.getLines(); l++) {
            for (int c = 0; c < model.getColumns(); c++) {
                cellViews[l][c] = CellView.newInstance(model.getCell(l,c));
                cellViews[l][c].paintDefaultCell();
            }
        }
    }

    // Converts Console mouse coordinates to game grid coordinates
    public Position getModelPosition(int line, int col) {
        int posLine = line/CellView.CELL_SIDE;
        int posCol = col/CellView.CELL_SIDE;

        if (posLine < 0 || posLine >= cellViews.length)
            return null;
        if (posCol < 0 || posCol >= cellViews[0].length)
            return null;

        return new Position(posLine, posCol);
    }

    public void paint(Position position, boolean b) {
        cellViews[position.getLine()][position.getCol()].paint(position.getLine(), position.getCol(), b);
    }

    public void repaint() {
        for (int l = 0; l < cellViews.length; l++) {
            for (int c = 0; c < cellViews[l].length; c++) {
                cellViews[l][c].paint(l,c, false);
            }
        }
    }

    // Initiates elapsed time counting for each level
    private void initTimer() {
        totalTime = seconds = minutes = 0;
        Console.cursor(cellViews.length * CellView.CELL_SIDE, 0);
        Console.color(Console.WHITE, Console.GRAY);
        Console.print(String.join("", Collections.nCopies(cellViews[0].length*CellView.CELL_SIDE, " ")));
    }

    // Updates elapsed time during game
    public void repaintTime() {
        Console.cursor(cellViews.length * CellView.CELL_SIDE, 0);
        Console.color(Console.WHITE, Console.GRAY);

        int delay = 1000000;

        long currentTime = System.nanoTime();
        if ( currentTime/1000 >= (startTime/1000 + delay)) {
            minutes = (totalTime % 3600) / 60;
            seconds = totalTime++ % 60;

            Console.print(String.format("%02d:%02d", minutes, seconds));

            startTime = currentTime;
        }
    }


    // Display a given message on the text area of the Console Window
    public void message(String msg) {
        clearTextArea();
        Console.cursor(cellViews.length * CellView.CELL_SIDE, 0);
        Console.color(Console.YELLOW, Console.GRAY);
        Console.print(msg);
        Console.sleep(500);
    }

    // Ask a question and wait for a Y/N response.
    public boolean question(String next) {
        message(next + "? (Y/N) ");
        Console.cursor(true);
        while (!Console.isKeyPressed()) {
            int key = Console.getKeyPressed();
            key = key >= 'a' && key <= 'z' ? key - 32 : key;
            if (key == KeyEvent.VK_N || key == KeyEvent.VK_ESCAPE)  // Return false on No or Esc key press
                return false;
            if (key == KeyEvent.VK_Y)                               // Return true on Yes
                break;
        }
        return true;
    }

    // Clears the text area of the Console Window
    private void clearTextArea() {
        Console.cursor(cellViews.length * CellView.CELL_SIDE, 0);
        Console.color(Console.YELLOW, Console.GRAY);
        StringBuilder out = new StringBuilder();
        int spaces = cellViews[0].length*CellView.CELL_SIDE;
        out.append(String.join("", Collections.nCopies(spaces, " ")));
        Console.print(out.toString());
        Console.cursor(false);
    }

    // Close current console window
    public void close() {
        Console.close();
    }
}
