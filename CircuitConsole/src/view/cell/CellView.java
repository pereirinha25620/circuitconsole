package view.cell;

import isel.leic.pg.Console;
import model.cell.*;
import model.Dir;

/**
 * Base class hierarchy of cell viewers
 */
public class CellView {
    public static final int CELL_SIDE = 3;  // Each side of the cell occupies 3 characters
    protected final int centerY;
    protected final int centerX;
    protected int color;

    // Matches between the color number in the model and the color displayed on the console
    private static final int[] COLORS = {
        Console.RED, Console.GREEN, Console.YELLOW,
        Console.BLUE, Console.MAGENTA, Console.ORANGE
    };

    protected final Cell cell; // Reference to the model cell

    CellView(Cell cell) {
        this.cell = cell;
        centerY = cell.getLine() * CELL_SIDE + 1;
        centerX = cell.getCol() * CELL_SIDE + 1;
    }

    /**
     * The color used to show the cell
     * @return The console color to use in that cell
     */
    int getColor() {
        if (cell.getColor() < 0 || cell.getColor() >= COLORS.length) {
            return Console.GRAY;
        }
        return COLORS[ cell.getColor() ];
    }

    /**
     * Creates the appropriate view of the cell type of the model
     * @param cell The model cell
     * @return The view for the cell
     */
    public static CellView newInstance(Cell cell) {
        if (cell instanceof Terminal) return new TerminalView(cell);
        if (cell instanceof HorLine) return new HorLineView(cell);
        if (cell instanceof VertLine) return new VertLineView(cell);
        if (cell instanceof Free) return new FreeView(cell);
        return new CellView(cell);
/*      try { // NOTE: Alternative implementation using reflexion
            Class vc = Class.forName("view.cell."+cell.getClass().getSimpleName()+"View");
            return (CellView) vc.getConstructor(Cell.class).newInstance(cell);
        } catch (Exception e) { return new CellView(cell); }
*/
    }

    /**
     * Helper method to write a char at the current position of the console with a background color
     * @param v Char to write
     * @param bc Background color to use
     */
    protected static void write(char v, int bc) {
        Console.setBackground(bc);
        Console.print(v);
    }

    /**
     * Paint the cell of a coordinate (of the model) with or without highlight.
     * @param l HorLine in model
     * @param c Column in model
     * @param highLight Is to present highlighted
     */
    public void paint(int l, int c, boolean highLight) {
    }

    /**
     * Paint a single square element char in CellView</br>
     * @param bkColor Background color that can be used
     */
    protected void paintElement(int bkColor) { write(' ',bkColor); }

    /**
     * Common method used for setting a CellView to it's default, unconnect state.
     * Specific implementations for each Cell type.
     */
    public void paintDefaultCell() {}
}
