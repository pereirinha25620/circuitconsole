package view.cell;

import isel.leic.pg.Console;
import model.Dir;
import model.cell.Cell;
import model.cell.Node;

public class VertLineView extends NodeView {

    public VertLineView(Cell cell) {
        super(cell);
    }

    public void paintDefaultCell() {
        color = Console.GRAY;
        paintCenter();
        paintSide(Dir.UP, color);
        paintSide(Dir.DOWN, color);
    }

    public void paint(int l, int c, boolean highLight) {
        paintDefaultCell();
        super.paint(l, c, highLight);
        int bkColor = highLight? Console.LIGHT_GRAY : Console.BLACK;
        Console.setBackground(bkColor);
        paintSide(Dir.LEFT,bkColor);
        paintSide(Dir.RIGHT,bkColor);
        paintCenter();

        for (Dir dir : cell.getConnections()) {
            paintSide(dir, getColor());
        }
    }
}
