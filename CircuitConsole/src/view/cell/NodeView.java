package view.cell;

import isel.leic.pg.Console;
import model.Dir;
import model.cell.Cell;

public abstract class NodeView extends CellView {

    public NodeView(Cell cell) {
        super(cell);
        color = getColor();
    }

    public void paintDefaultCell() {
        color = Console.GRAY;
        paintCenter();
    }

    public void paint(int l, int c, boolean highLight) {
        l *= CELL_SIDE; l++;
        c *= CELL_SIDE; c++;
        int bkColor = highLight? Console.LIGHT_GRAY : Console.BLACK;
        Console.setBackground(bkColor);
        Console.cursor(l-1,c-1); paintElement(bkColor);
        Console.cursor(l-1,c+1); paintElement(bkColor);
        Console.cursor(l+1,c-1); paintElement(bkColor);
        Console.cursor(l+1,c+1); paintElement(bkColor);
    }


    protected void paintCenter() {
        Console.cursor(centerY, centerX);
        write(' ', getColor());
    }

    protected void paintSide(Dir dir, int newColor) {
        Console.cursor(centerY + (dir.getDeltaLin()), centerX + (dir.getDeltaCol()));
        paintElement(newColor);
    }

}
