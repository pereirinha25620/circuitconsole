package view.cell;

import isel.leic.pg.Console;
import model.cell.Cell;

import java.util.Collections;

public class TerminalView extends CellView {

    public TerminalView(Cell cell) {
        super(cell);
    }

    public void paintDefaultCell() {
        Console.color(Console.WHITE, getColor());
        for (int l = centerY - 1; l < centerY - 1 + CELL_SIDE; l++) {
            Console.cursor(l, centerX - 1);
            Console.print(String.join("", Collections.nCopies(CELL_SIDE, " ")));
        }
        Console.cursor(centerY, centerX);
        write('O', getColor());
    }
}
