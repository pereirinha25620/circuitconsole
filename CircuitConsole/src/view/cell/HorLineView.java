package view.cell;


import isel.leic.pg.Console;
import model.Dir;
import model.cell.Cell;
import model.cell.Node;

public class HorLineView extends NodeView {

    public HorLineView(Cell cell) {
        super(cell);
    }

    public void paintDefaultCell() {
        color = Console.GRAY;
        paintCenter();
        paintSide(Dir.LEFT, color);
        paintSide(Dir.RIGHT, color);
    }

    public void paint(int l, int c, boolean highLight) {
        paintDefaultCell();
        super.paint(l, c, highLight);
        int bkColor = highLight? Console.LIGHT_GRAY : Console.BLACK;
        Console.setBackground(bkColor);
        paintSide(Dir.UP,bkColor);
        paintSide(Dir.DOWN,bkColor);
        Console.setBackground(getColor());
        paintCenter();

        for (Dir dir : cell.getConnections()) {
            paintSide(dir, getColor());
        }
    }

}
