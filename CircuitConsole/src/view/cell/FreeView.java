package view.cell;

import isel.leic.pg.Console;
import model.Dir;
import model.cell.Cell;
import model.cell.Node;

public class FreeView extends NodeView {

    public FreeView(Cell cell) {
        super(cell);
    }

    public void paint(int l, int c, boolean highLight) {
        super.paint(l, c, highLight);
        int bkColor = highLight? Console.LIGHT_GRAY : Console.BLACK;
        Console.setBackground(bkColor);
        paintSide(Dir.LEFT,bkColor);
        paintSide(Dir.UP,bkColor);
        paintSide(Dir.RIGHT,bkColor);
        paintSide(Dir.DOWN,bkColor);

        paintCenter();

        for (Dir dir : cell.getConnections()) {
            paintSide(dir, getColor());
        }
    }
}
