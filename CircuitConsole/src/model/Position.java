package model;

public class Position {

    private int line;
    private int col;

    public Position(int newLine, int newCol) {
        line = newLine;
        col = newCol;
    }

    public int getLine() {
        return line;
    }

    public int getCol() {
        return col;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof Position) {
            Position other = (Position) obj;
            if (other.getLine() == line && other.getCol() == col) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Pos: (" + line + ";" + col + ")";
    }
}
