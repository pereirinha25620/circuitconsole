package model.cell;

import model.Dir;
import model.Position;

public class HorLine extends Node {

    public HorLine(char type, Position pos) {
        super(type, pos);
    }

    @Override
    public boolean drag(Dir dir) {
        return dir == Dir.LEFT || dir == Dir.RIGHT;
    }
}
