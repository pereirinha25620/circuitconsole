package model.cell;

import model.Dir;
import model.Position;

public class VertLine extends Node {

    public VertLine(char type, Position pos) {
        super(type, pos);
    }

    @Override
    public boolean drag(Dir dir) {
        return dir == Dir.UP || dir == Dir.DOWN;
    }
}
