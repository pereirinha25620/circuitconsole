package model.cell;

import model.Dir;
import model.Position;

public class Free extends Node {

    public Free(char type, Position pos) {
        super(type, pos);
    }

    @Override
    public boolean drag(Dir dir) {
        return true;
    }

}
