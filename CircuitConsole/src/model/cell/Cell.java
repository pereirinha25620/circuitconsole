package model.cell;

import model.Dir;
import model.Position;

import java.util.LinkedList;

public abstract class Cell {

    protected char type;
    protected Position pos;
    protected LinkedList<Dir> connections;

    public static Cell newInstance(char cellType, Position pos) {
        if (cellType == '.')
            return new Free(cellType, pos);
        if (cellType == '-')
            return new HorLine(cellType, pos);
        if (cellType == '|')
            return new VertLine(cellType, pos);
        if (cellType >= 'A' && cellType <= 'F')
            return new Terminal(cellType, pos);
        return null;
    }


    public Cell(char type, Position pos) {
        this.type = type;
        this.pos = pos;
        connections = new LinkedList<>();
    }


    // Return cell "type" as an integer to be used as index in COLOR array
    public int getColor() {
        return type - 'A';
    }



    // Return cell coordinates in model
    public Position getPos() {
        return pos;
    }

    public int getLine() {
        return pos.getLine();
    }

    public int getCol() {
        return pos.getCol();
    }


    // ABSTRACT CELL METHODS
    public abstract boolean isConnected();

    public abstract boolean connectCell(Dir dir);

    public abstract boolean unlinkCell(Dir dir);

    public abstract Dir getNextDirection(Dir dir);

    public abstract void resetCell();

    public abstract boolean drag(Dir dir);


    public Dir getConnection() {
        return connections.getLast();
    }

    public LinkedList<Dir> getConnections() {
        return connections;
    }

    public boolean isInPath() {
        return !connections.isEmpty();
    }

    public char getType() {
        return type;
    }

}
